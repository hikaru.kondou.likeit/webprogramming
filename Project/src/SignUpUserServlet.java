

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class SignUpUserServlet
 */
@WebServlet("/SignUpUserServlet")
public class SignUpUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SignUpUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User userInfo = (User)session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}


		request.setCharacterEncoding("UTF-8");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUpUser.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password =request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String UserName = request.getParameter("name");
		String birth = request.getParameter("birthDate");

		User user = new User();
		UserDao userDao = new UserDao();
		user = userDao.findByLoginId(loginId);


		if(!(user == null)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUpUser.jsp");
			dispatcher.forward(request, response);
			return;

		}

		if(!(password.equals(passwordConfirm))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUpUser.jsp");
			dispatcher.forward(request, response);
			return;

		}

		if(loginId.equals("") || UserName.equals("") || birth.equals("") || password.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/SignUpUser.jsp");
			dispatcher.forward(request, response);
			return;

		}



		UserDao userDAO = new UserDao();
		userDAO.SignUp(loginId,password,UserName,birth);

		response.sendRedirect("UserlistServlet");


	}

}
