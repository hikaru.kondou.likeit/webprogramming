<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 <style>
   div {
    text-align: center;
   }
     *{
         margin:0px;
         padding:0px;
     }

 </style>

</head>
<body>

    <div class="container">

  <div style="height:30px; background-color: #555555	;">
    <div class="text-muted">
      <p class="mr-3" style="text-align: right">${sessionScope.userInfo.name}<a href="LogoutServlet" class="ml-3">ログアウト</a></p>

    </div>
  </div>
  <h1 style="text-align: center">ユーザ情報更新</h1>
      <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	  </c:if>


  <div>

 	  <p>${user.loginId}</p>
 	  <form action="UpDateUserInfo" method = "post">
 	  <input name="id" value="${user.id}" type="hidden">
      <p>パスワード <input class="ml-5" type="password"name="password"></p>
      <p>パスワード(確認) <input class="ml-5" type="password"name="passwordConfirm"></p>
      <p>ユーザ名 <input class="ml-5" value="${user.name}"  type="text"name="name"></p>
      <p>生年月日 <input class="ml-5" value="${user.birthDate}"  type="date"name="birthDate"></p>
      <p><input class="ml-5" type="submit"value="更新"></p>
      </form>
      <div style="text-align: left"><p><a href="UserlistServlet">戻る</a></p></div>
  </div>
    </div>
</body>
</html>