
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
    <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

 <style>
     div {

      text-align: center;
     }
     h1 {
      text-align: center;
      padding: 20;
     }

     p {
         text-align: center;
         padding: 20;
     }

 </style>

</head>

<body>
    <div class="container">
    <div  style="height:30px; background-color: #555555	;">
        <div class="text-muted">
            <p class="mr-3" style="text-align: right">${sessionScope.userInfo.name}<a href="LogoutServlet" class="ml-3">ログアウト</a></p>

        </div>
    </div>

     <h1>ユーザ情報詳細参照</h1>
	   <div>
    <p>${user.loginId}</p>
	<p>${user.name}</p>
    <p>${user.birthDate}</p>
    <p>${user.createDate}</p>
    <p>${user.updateDate}</p>
    <div class="float-left"><p><a href="UserlistServlet">戻る</a></p></div>
	</div>
    </div>

</body>
</html>