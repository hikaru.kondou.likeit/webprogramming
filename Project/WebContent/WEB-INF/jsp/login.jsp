<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
     <title>ログイン画面</title>

     <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

       <style>
       	 h1 {
       	 text-align: center;
       	 }

        	div {
			text-align:center;
            padding: 10px;
        	}

           p {
               padding: 20px;
           }
        </style>
</head>
<body>

 <h1>ログイン画面</h1>

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

   <div>
	  <form action="LoginServlet" method="post">
       <p>ログインID　<input type="text"name="loginId"></p>
       <p>パスワード　<input type="password"name="password"></p>
 	   <p><input type="submit"value="ログイン" onclick="location.href='User.html'"></p>
 	  </form>
 	</div>
</body>
</html>