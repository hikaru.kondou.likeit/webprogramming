<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<style>
div {
	text-align: center;
}

h1 {
	text-align: center;
	padding: 30px;
}

p {
	text-align: center;
	padding: 20;
}
</style>
</head>
<body>

	<div class="container">
		<div style="height: 30px; background-color: #555555;">
			<div class="text-muted">
				<p class="mr-3" style="text-align: right">${sessionScope.userInfo.name}<a
						href="LogoutServlet" class="ml-3">ログアウト</a>
				</p>

			</div>
		</div>

		<h1>ユーザ新規登録</h1>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
    </div>



		<div>
			<form action="SignUpUserServlet" method="post">
				<p>
					ログインID <input class="ml-5" type="text" name="loginId">
				</p>
				<p>
					パスワード <input class="ml-5" type="password" name="password">
				</p>
				<p>
					パスワード(確認) <input class="ml-5" type="password"
						name="passwordConfirm">
				<p />
				<p>
					ユーザ名 <input class="ml-5" type="text" name="name">
				</p>
				<p>
					生年月日 <input class="ml-5" type="date" name="birthDate">
				</p>
				<p>
					<input type="submit" value="登録" onclick="location.href='User.html'">
				</p>
			</form>
			<div class="float-left">
				<p>
					<a href="UserlistServlet">戻る</a>
				</p>
			</div>
		</div>
</body>
</html>