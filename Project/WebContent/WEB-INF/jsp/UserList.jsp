<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"></head>
    <style>


        p {
            text-align: right;
        }

    </style>
<body>


<div class="container">
    <div style="height:30px; background-color: #555555	;">
        <div class="text-muted">
            <p class="mr-3">${sessionScope.userInfo.name}さん<a href="LogoutServlet" class="ml-3">ログアウト</a></p>
        </div>
    </div>
	<h1 style="text-align:center;">ユーザー覧</h1>
     <p><a href="SignUpUserServlet">新規登録</a></p>
     <form action="UserlistServlet"  method="post">
      <div class="col-5" style="margin-right:auto; margin-left:auto;">
          <p>ログインID <input class="ml-5" type="text"name="loginId"></p><br>
          <p>ユーザー名 <input class="ml-5" type="text"name="name"></p><br>
          <p>生年月日 <input class="ml-4" type="date" name ="birthDateS">~<input type="date"name ="birthDateE"></p>
      </div>
     <div><input class="float-right mr-5" type="submit"value="検索"></div>
      </form>



        <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
                      <c:if test="${sessionScope.userInfo.loginId == 'admin'}">
                       <a class="btn btn-primary" href="UserInfoServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UpDateUserInfo?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDelete?id=${user.id}">削除</a>
                      </c:if>
                      <c:if test="${sessionScope.userInfo.loginId != 'admin'}">
                       <a class="btn btn-primary" href="UserInfoServlet?id=${user.id}">詳細</a>
                       <c:if test="${sessionScope.userInfo.loginId == user.loginId}">
                        <a class="btn btn-success" href="UpDateUserInfo?id=${user.id}">更新</a>
                       </c:if>
                      </c:if>
                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>


</div>

</body>
</html>